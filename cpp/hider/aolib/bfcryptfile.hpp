/*
    Copyright (C) 2004 Adam Olsen

	This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 1, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/
#ifndef BFCRYPT_FILE_HPP
#define BFCRYPT_FILE_HPP

#include <openssl/rand.h>
#include <openssl/evp.h>
#include <sys/stat.h>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <string>
#include <string.h>
#include <sstream>

#ifndef EVP_MAX_BLOCK_LENGTH
#define EVP_MAX_BLOCK_LENGTH 32
#endif

namespace aolib
{
	/**
	 * Takes the specified file "inFile" and encrypts or decrypts it to "outFile", using the specified
	 * password as the key.  Each time the file is encrypted, a new initialization vector for Blowfish and a new
	 * salt for SHA-1 are created from random bits and written out to the encrypted file. These two are then read off of the top
	 * of the file for decryption.
	 * @author Adam Olsen
	**/
	class BFCryptFile
	{
		private:
			/**
			 * Encrypts or decrypts ifile to ofile according to encordec, using pass, salt, and ivec.  If any problems occur,
			 * a BFCryptException will be thrown
			**/
			static void cryptFile( std::ifstream &ifile, std::ofstream &ofile, std::string pass, unsigned char *salt,
				unsigned char *ivec, int encordec, int fileLength );

		public:
			/**
			 * The encsig is an implemntation of "magic numbers".  25 bytes of seemingly random characters
			 * are written to the top of an encrypted file.  When decrypting, if these bytes aren't found,
			 * then we know that it's not an encrypted file (at least not one encrypted by this class).
			**/
			static const unsigned char encsig[];

			/**
			 * Hashes the specified string "pass" one time with sha-1, and passes back only the first 16 bits for
			 * storeage in the encrypted file.  What this does is it leads to any dictionary attacks appearing to
			 * be accepted (1 out of 65536 will appear correct) until the decryption fails
			**/
			static std::string hashPassOnce( const char *pass );

			/**
			 * Hashes the specified string "pass" 10000 times with SHA-1 using "salt" as the salt.
			**/
			static std::string hashPass( const char *pass, unsigned char *salt );

			/**
			 * Zeros out a file by writing '\0' to every byte, and then unlinks it
			**/
			static void shredFile( const char *file );

			/**
			 * Encrypts inFile to outFile. If any errors occur, a BFCryptException will be thrown
			**/
			static void encryptFile( std::string username, std::string password, std::string inFile, std::string outFile );

			/**
			 * Decrypts inFile to outFile.If any errors occur, a BFCryptException will be thrown
			**/
			static void decryptFile( std::string username, std::string password, std::string inFile, std::string outFile );

			static bool checkSig( std::string file );
	};

	class BFCryptException
	{
		private:
			std::string message;
		public:
			BFCryptException( const char *error ) { this->message = error; }
			BFCryptException( std::string error ) { this->message = error; }
			std::string what() { return message; }
	};
}

#endif // BFCRYPT_FILE_HPP
