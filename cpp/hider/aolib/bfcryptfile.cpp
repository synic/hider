/*
    Copyright (C) 2004 Adam Olsen

	This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 1, or (at your option)
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#include <aolib/bfcryptfile.hpp>
#include <openssl/sha.h>
#include <stdlib.h>

namespace aolib
{
	// set up the magic numbers
	const unsigned char BFCryptFile::encsig[25] = { 0, 2, 144, 4, 3, 4, 5, 6, 3, 12, 144, 134, 5, 6, 24, 2, 0, 3, 4, 3, 4, 3, 88, 4, 5 };

	std::string BFCryptFile::hashPassOnce( const char *pass )
	{
		std::string hash;

		unsigned char md_value[EVP_MAX_MD_SIZE] = { '\0' };
		char temp[2] = { '\0' };
		EVP_MD_CTX mdctx;
		unsigned int md_len;

		// initialize the SHA1 algorithm
		EVP_DigestInit( &mdctx, EVP_sha1() );

		// hash the pass
		EVP_DigestUpdate( &mdctx, pass, strlen( pass ) );

		// finalize the hash
		EVP_DigestFinal( &mdctx, md_value, &md_len );

		bzero( temp, 2 );
		sprintf( temp, "%02x", md_value[0] );
		hash +=  temp;
		sprintf( temp, "%02x", md_value[1] );
		hash +=  temp;

		return hash;
	}

	std::string BFCryptFile::hashPass( const char *pass, unsigned char *salt )
	{
		std::string hash;

		char temp[2] = { '\0' };
		unsigned char out[SHA_DIGEST_LENGTH] = { '\0' };

		// hash the password 10000 times using the salt
		PKCS5_PBKDF2_HMAC_SHA1( pass, strlen( pass ), salt, 8, 10000, SHA_DIGEST_LENGTH, out );

		// convert the result into a hex-string representation
		for( int i = 0; i < SHA_DIGEST_LENGTH; i++ )
		{
			bzero( temp, 2 );
			sprintf( temp, "%02x", out[i] );
			hash += temp;
		}

		return hash;
	}

	bool BFCryptFile::checkSig( std::string inFile )
	{
		char user[1024] = { '\0' };
		char pass[5] = { '\0' };
		char salt[8] = { '\0' };
		char ivec[8] = { '\0' };
		unsigned char checksig[25];
		std::ifstream in( inFile.c_str(), std::ios::binary );

		if( !in.is_open() )
		{
			throw( BFCryptException( "Could not open input file." ) );
		}

		// get the various required bytes from the file for decryption
		in.getline( user, 1024 );
		in.getline( pass, 5 );
		in.read( (char *)checksig, 25 );
		in.read( salt, 8 );
		in.read( ivec, 8 );
		in.close();

		// check to make sure the magic numbers were present
		if( strcmp( (char *)encsig, (char *)checksig ) != 0 )
		{
			return true;
		}

		return false;
	}

	void BFCryptFile::decryptFile( std::string username, std::string password, std::string inFile, std::string outFile )
	{
		char user[1024] = { '\0' };
		char pass[5] = { '\0' };
		char salt[8] = { '\0' };
		char ivec[8] = { '\0' };
		unsigned char checksig[25];

		std::ifstream in( inFile.c_str(), std::ios::binary );

		in.seekg( 0, std::ios::end );
		int fileLength = in.tellg();
		in.seekg( 0, std::ios::beg );

		if( !in.is_open() )
		{
			throw( BFCryptException( "Could not open input file." ) );
		}

		// get the various required bytes from the file for decryption
		in.getline( user, 1024 );
		in.getline( pass, 5 );
		in.read( (char *)checksig, 25 );
		in.read( salt, 8 );
		in.read( ivec, 8 );

		// check to make sure the magic numbers were present
		if( strcmp( (char *)encsig, (char *)checksig ) != 0 )
		{
			throw( BFCryptException( "This file does not appear to be an encrypted file, or it has been corrupted." ) );
		}

		// do a light check to make sure the password at least appears to be correct
		if( strcmp( hashPassOnce( password.c_str() ).c_str(), pass ) != 0 )
		{
			throw( BFCryptException( "You have entered an invalid username and/or password." ) );
		}

		// make sure the username is correct
		if( strcmp( username.c_str(), user ) != 0 )
		{
			throw( BFCryptException( "You have entered an invalid username and/or password." ) );
		}

		std::ofstream out( outFile.c_str(), std::ios::binary );
		if( !out.is_open() )
		{
			throw( BFCryptException( "Could not open temp file for writing." ) );
		}

		#ifndef WIN32
			chmod( outFile.c_str(), 0600 );
		#endif

		// and decrypt the file using the salt and ivec found in the file
		cryptFile( in, out, password, (unsigned char *)salt, (unsigned char *)ivec, 0, fileLength );
	}

	void BFCryptFile::encryptFile( std::string username, std::string password, std::string inFile, std::string outFile )
	{
		char salt[8] = { '\0' };
		char ivec[8] = { '\0' };

		// create a random ivec and salt
		RAND_bytes( (unsigned char *)salt, 8 );
		RAND_bytes( (unsigned char *)ivec, 8 );

		std::ifstream in( inFile.c_str(), std::ios::binary );
		in.seekg( 0, std::ios::end );
		int fileLength = in.tellg();
		in.seekg( 0, std::ios::beg );

		if( !in.is_open() )
		{
			throw( BFCryptException( "Could not open input file for reading." ) );
		}

		std::ofstream out( outFile.c_str(), std::ios::binary );
		if( !out.is_open() )
		{
			throw( BFCryptException( "Could not open input file for writing." ) );
		}

		// write the username, the hashed password, the salt, the encryption
		// signature, and the ivec to the file
		out << username << std::endl;
		out << hashPassOnce( password.c_str() ) << std::endl;
		out.write( (char *)encsig, 25 );
		out.write( salt, 8 );
		out.write( ivec, 8 );

		#ifndef WIN32
			chmod( outFile.c_str(), 0600 );
		#endif

		// encrypt the rest of the data and write it to the output file
		cryptFile( in, out, password, (unsigned char *)salt, (unsigned char *)ivec, 1, fileLength );
	}

	void BFCryptFile::cryptFile( std::ifstream &ifile, std::ofstream &ofile, std::string pass, unsigned char *salt,
		unsigned char *ivec, int encordec, int fileLength )
	{
		unsigned char in[1042];
		unsigned char out[1024 + EVP_MAX_BLOCK_LENGTH];
		unsigned long size = 0;
		int outlen = 0;
		int progressCounter = 0;

		std::string typeString = "Encrypting";
		if( !encordec ) typeString = "Decrypting";

		pass = hashPass( pass.c_str(), salt );

		EVP_CIPHER_CTX ctx;
		EVP_CIPHER_CTX_init( &ctx );

		// initialize the encrypt functions to use blowfish
		if( !EVP_CipherInit( &ctx, EVP_bf_cbc(), NULL, NULL, encordec ) )
		{
			throw( BFCryptException( "OpenSSL error setting ctx key." ) );
		}

		// set the blowfish key length
		EVP_CIPHER_CTX_set_key_length( &ctx, pass.length() );

		// set the key and the initialization vector
		if( !EVP_CipherInit( &ctx, NULL, (unsigned char *)pass.c_str(), ivec, encordec ) )
		{
			throw( BFCryptException( "OpenSSL error setting ctx key." ) );
		}

		int markCount = 40;
		int marker = fileLength / markCount;
		int markers = 0;
		int mark = 0;
		int currentSize = 0;
		int updateCount = -1;

		float percent = 0.0;

		while( !ifile.eof() )
		{
			bzero( in, 1024 );
			bzero( out, 1024 );
			// read in data from the in file

			ifile.read( (char *)in, 1024 );
			size = ifile.gcount();

			if( size )
			{
				// encrypt/decrypt the data
				if( !EVP_CipherUpdate( &ctx, out, &outlen, in, size ) )
				{
					throw( BFCryptException( "OpenSSL error while decrypting/encrypting file.  It's possible that your password was bad, or the file was corrupted." ) );
				}

				// write the new data to the output file
				ofile.write( (char *)out, outlen );

				currentSize += size;
			}

			percent = ( (float)currentSize / (float)fileLength );
			if( !size || ifile.eof() ) percent = 1;

			mark += size;
			if( mark > marker )
			{
				markers++;
				mark = 0;
			}

			if( markers == markCount - 1 ) percent = 1;

			if( updateCount > 100 || updateCount == -1 )
			{
				std::cout << "\r" + typeString + ": [";
				for( int i = 0; i <= markers; i++ ) std::cout << "#";
				for( int i = markers; i < markCount - 1; i++ ) std::cout << " ";
				std::cout << std::flush << "] " << (int)( percent * 100 ) << "\%\r";
				updateCount = 0;
			}
			updateCount++;
		}

		std::cout << "\r" + typeString + ": [";
		for( int i = 0; i <= markCount; i++ ) std::cout << "#";
		std::cout << "] " << (int)( percent * 100 ) << "\%\r";

		// finalize the encryption (this adds padding where it is needed
		if( !EVP_CipherFinal( &ctx, out, &outlen ) )
		{
			throw( BFCryptException( "OpenSSL error while decrypting/encrypting file.  It's possible that your password was bad, or the file was corrupted." ) );
		}

		// write the finalized/padded info to the file
		ofile.write( (char *)out, outlen );

		EVP_CIPHER_CTX_cleanup( &ctx );
		ifile.close();
		ofile.close();
	}

	void BFCryptFile::shredFile( const char *file )
	{
		std::ifstream in( file, std::ios::binary );
		if( !in.is_open() ) return;

		// find out the file size
		in.seekg( 0, std::ios::end );
		unsigned int fileLength = in.tellg();
		in.seekg( 0, std::ios::beg );
		in.close();

		// write zeros the length of the file
		std::ofstream out( file, std::ios::binary );
		if( !out.is_open() ) return;

		for( unsigned int i = 0; i < fileLength; i++ )
		{
			out.put( '\0' );
		}

		out.close();

		// delete the file
		unlink( file );
	}
}
