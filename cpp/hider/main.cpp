#include <unistd.h>
#include <errno.h>
#include <iostream>
#include <aolib/bfcryptfile.hpp>
#include <string>
#include <map>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <time.h>
#include <sstream>


using namespace std;

map<string, string> options;
map<string, string> settings;
const string VERSION = ".01b";

void error( string message )
{
	cout << message << endl << endl;
	exit( -1 );
}

void setUpOptions()
{
	options["h"] = "-";
	options["help"] = "-";
	options["X"] = "-";
	options["v"] = "-";
	options["o"] = "reqoption";
	options["p"] = "optionaloption";
	options["u"] = "reqoption";
	options["S"] = "-";
	options["d"] = "-";
}

void showUseage()
{
	cout << "Useage: hider [options] inputfile" << endl;
	cout << "Valid options are:" << endl << endl;
	cout << "\t-o\tThe output file (not required, an automatic one \n\t\twill be generated)" << endl << endl;
	cout << "\t-p\tThe password (if not specified on the command line, you \n\t\twill be prompted)" << endl << endl;
	cout << "\t-u\tThe username.  It is not required, and if not \n\t\tyou will be provided a default will be used ";
	cout << "\n\t\t(this does not effect security much.  You should \n\t\treally be trying to choose a good password)\n\t\t" << endl;
	cout << "\t-h\tThis will simply show you this message." << endl << endl;
	cout << "\t-v\tShow the version number of this software," << endl << endl;
	cout << "\t-d\tDelete the input file after the output file is created.\n\t\tThis is simply done with an unlink.";
	cout << "If you require \n\t\tmore security, use -S instead." << endl << endl;
	cout << "\t-X\tWrite output to the original file \n\t\t(can only be used if you did not specify -o) " << endl << endl;
	cout << "\t-S\tShreds the input file leaving only the output file.  \n\t\tNote that this can take a long time depending on\n\t\tthe file size.  It normally takes ";
	cout << "about the same amount \n\t\tof time it took to encrypt the file." << endl << endl;

	cout << endl;
	exit( -1 );
}

bool fileExists( string filename )
{
	ifstream in( filename.c_str(), ios::in );
	bool exists = false;

	if( in.is_open() )
	{
		exists = true;
		in.close();
	}
	return exists;
}

void showVersion()
{
	cout << "Adam's encryptor v" << VERSION << " (c)2004" << endl << endl;
	exit( 0 );
}

bool checkOption( string arg, int &num, int argc, char **argv, int &argcount )
{
	if( arg == "h" ) showUseage();
	if( arg == "v" ) exit( 0 );
	string type = options[arg];
	if( type == "" ) error( "Invalid option: \"" + arg + "\".  Please try -h for help." );

	if( type.find( "option" ) != string::npos )
	{
		if( num < argc && argv[num + 1][0] != '-' )
		{
			settings[arg] = argv[num + 1];
			argcount--;
		}
		else if( type.find( "reqoption" ) == string::npos ) return false;
	}
	else settings[arg] = "_ ok _";

	return true;
}

void checkPasswords()
{
	if( settings["u"] == "_ ok _" && settings["u"] != "" )
	{
		cout << "Enter the username for this archive: ";
		cin >> settings["u"];
	}

	if( settings["p"] == "" )
	{
		string courtesy = "";

		if( settings["method"] == "encrypt" ) courtesy = "you would like to use";

		char *pass = getpass( ( "Please enter the password " + courtesy + "for this archive: " ).c_str() );
		string password = pass;
		if( password == "" ) error( "Don't enter an empty password." );

		if( settings["method"] == "encrypt" )
		{
			pass = getpass( "For security sake, please verify your new password: " );
			string verify = pass;
			if( password != verify )
			{
				cout << "Password and verification don't match." << endl << endl;
				checkPasswords();
			}
		}

		settings["p"] = password;
	}
}

void parseArgs( int argc, char **argv )
{
	if( argc <= 1 ) showUseage();

	int argCount = -1;
	for( int i = 1; i < argc; i++ )
	{
		string arg = argv[i];
		if( arg.substr( 0, 1 ) == "-" )
		{
			string temp;

			for( int a = 1; a < arg.length(); a++ )
			{
				temp += arg[a];
			}

			bool ok = checkOption( temp, i, argc, argv, argCount );
			if( !ok ) error( "Sorry, but the argument \"" + arg + "\" requires a parameter." );
		}
		else argCount++;
	}

	string infile = argv[argc - 1];
	if( infile == "" || argCount ) error( "Invalid syntax.  Try -h" );

	settings["i"] = infile;

	if( argCount > 0 ) showUseage();

	try {
		if( aolib::BFCryptFile::checkSig( settings["i"] ) )
		{
			settings["method"] = "encrypt";
		}
		else settings["method"] = "decrypt";
	}
	catch( aolib::BFCryptException ex )
	{
		error( ex.what() );
	}

	if( settings["method"] == "encrypt" )
	{
		cout << "This is not an encrypted file.  Using encryption mode." << endl;
	}
	else {
		cout << "This is an encrypted file.  Using decryption mode." << endl;
	}

	if( settings["X"] != "" )
	{
		if( settings["o"] != "" ) error( "Sorry, you cannot specify an output file in conjunction with the -X option." );
		settings["o"] = ".tmp__" + settings["i"];
	}

	if( settings["o"] == "" )
	{
		if( settings["method"] == "encrypt" ) settings["o"] = settings["i"] + ".enc";
		else settings["o"] = settings["i"] + ".dec";
	}

	if( fileExists( settings["o"] ) ) error( "The output file \"" + settings["o"] +
		"\". already exists.\nI won't overwrite it so I'm bailing out.\nTry using the -o switch to specify a different file name." );

	map<string, string>::iterator i;
	for( i = options.begin(); i != options.end(); i++ )
	{
		string key = i->first;
		string value = i->second;

		if( value.substr( 0, 2 ) == "r " && settings[key] == "" )
		{
			cout << "Required parameter " + key + " was not specified." << endl;
			showUseage();
		}
	}

	if( settings["u"] == "" ) settings["u"] = "defaultusername";
	checkPasswords();
}

int main( int argc, char **argv )
{
	// a quick blurb about who wrote the app
	cout << "Blowfish encryption utility v" + VERSION + " (c)2004 by Adam Olsen (synic)" << endl << endl;

	setUpOptions();
	parseArgs( argc, argv );

	try {
		if( settings["method"] == "encrypt" ) aolib::BFCryptFile::encryptFile( settings["u"], settings["p"], settings["i"], settings["o"] );
		else aolib::BFCryptFile::decryptFile( settings["u"], settings["p"], settings["i"], settings["o"] );
	}
	catch( aolib::BFCryptException ex )
	{
		cout << "*** Failed." << endl;
		cout << "Sorry, there was an error " + settings["method"] + "ing your file." << endl;
		cout << ex.what() << endl << endl;
		return -1;
	}

	cout << endl;

	if( settings["X"] != "" )
	{
		int res = rename( settings["o"].c_str(), settings["i"].c_str() );

		if( res == -1 )
		{
			cout << "Failed to rename temp file \"" + settings["o"] + "\".  Error code: " << errno << endl;
			exit( -1 );
		}

		settings["o"] = settings["i"];
	}
	else if( settings["S"] != "" )
	{
		cout << "Shredding original input file - one moment..." << endl;;
		aolib::BFCryptFile::shredFile( settings["i"].c_str() );
		cout << "Done." << endl;
	}
	else if( settings["d"] != "" ) unlink( settings["i"].c_str() );

	cout << endl << "Success!  Your " + settings["method"] + "ed output was saved to " + settings["o"] << endl << endl;

	return 0;
}
